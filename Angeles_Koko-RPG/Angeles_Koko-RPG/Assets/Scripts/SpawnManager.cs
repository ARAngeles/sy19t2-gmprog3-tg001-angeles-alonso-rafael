﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : Singleton<SpawnManager>
{
    //use singleton for the spawner as long the spawner is a single instance in the game.

    // the other one is EventLister
    // you can add a event for each food when they destroyed you can broadcast a Action in your code


    public Transform minSpawnPoint;
    public Transform maxSpawnPoint;
    public int maxSpawned;
    int spawned;
    public GameObject objects;
    public List<GameObject> objectList;

    void Start()
    {
        for (int i = 0; i < maxSpawned; i++)
        {

            SpawnObjects();
            spawned++;
            objectList.Add(objects);

        }

        //create a loop that spawn 30 food during initialization
        // increase your food counter when you spawn a food
    }

    void Update()
    {
        //create a counter for food if you reach the max food count stop spawning of food
        //create a timer that spawn a food
    }

    void SpawnObjects()
    {
        float x = Random.Range(minSpawnPoint.position.x, maxSpawnPoint.position.x);
        float y = Random.Range(minSpawnPoint.position.y, maxSpawnPoint.position.y);
        Vector3 spwnPos = new Vector3(x, y, 0);
        var newObjs = GameObject.Instantiate(objects, spwnPos, Quaternion.identity);
    }

    public void RemoveObjects(GameObject objects)
    {
        spawned--;
        Destroy(objects.gameObject);
    }
    public void RemoveCollider(CircleCollider2D collider)
    {
        Destroy(collider);
    }
}
