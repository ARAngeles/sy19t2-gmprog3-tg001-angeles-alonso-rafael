﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class KokoScript : MonoBehaviour
{
    public LayerMask groundLayer;
    private NavMeshAgent kokoAgent;
    public CursorScript cursor;
    public GameObject currentPointer;
    private Animator animator;
    public
    float distance;
    bool isRunning = false;
    bool isStanding = true;
    RaycastHit hitPosition;

    enum KokoState
    {
        Idle,
        Running
    }
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        kokoAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isStanding == true)
        {
            animator.SetBool("isStandingBy", true);
        }
        else
        {
            animator.SetBool("isStandingBy", false);
        }

        if (isRunning == true)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
        }

        if (Input.GetMouseButtonDown(0))
        {
            isStanding = false;
            isRunning = true;
            Ray moveRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(moveRay, out hitPosition, 100, groundLayer))
            {

                //GameObject.Instantiate(point, hitPosition.point, Quaternion.identity);
                cursor.SpawnPointer(hitPosition.point);
                kokoAgent.SetDestination(hitPosition.point);

            }
        }

        distance = Vector3.Distance(hitPosition.point, transform.position);

        if (distance < 0.1)
        {
            isRunning = false;
            isStanding = true;
            //cursor.DestroyPointer();
        }

    }
}
