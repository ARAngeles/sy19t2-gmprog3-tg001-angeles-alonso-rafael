﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetectorScript : MonoBehaviour
{
    public float locationTime = 3;
    private float curLocationTime;
    Vector3 place;
    Vector3 movement;
    Vector3 border;

    [SerializeField] enemyStates currentState;
    public enum enemyStates
    {
        Run,
        Chase,
        Patrol
    }
  
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (body == null)
        {
            Destroy(this);
        }

        switch (currentState)
        {
            case enemyStates.Patrol:
                if (isColliding == false)
                {
                    randomMovement();
                }

                break;
            case enemyStates.Run:

                runAway(unit);
                break;
            case enemyStates.Chase:

                chasing(unit);
                break;
            default:
                currentState = enemyStates.Patrol;
                break;
        }


        // clamp the trasform position of the enemy 
        // you can use Mathf.clamp for the bounding part of the world


        float x = Mathf.Clamp(transform.position.x, -30, 30);
        float y = Mathf.Clamp(transform.position.y, -30, 30);

        this.transform.position = new Vector3(x, y, this.transform.position.z);

    }
}
