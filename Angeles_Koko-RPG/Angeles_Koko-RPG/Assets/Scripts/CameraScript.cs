﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public Transform target;
    Vector3 offset = new Vector3(57, 12, 16);
    public float moveSpd = 0.125f;
    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        Vector3 movementPositon = target.position;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, movementPositon, moveSpd);
        if (Input.GetMouseButtonDown(0))
        {
            transform.position = smoothedPosition + offset;

            

        }
       

    }
}
