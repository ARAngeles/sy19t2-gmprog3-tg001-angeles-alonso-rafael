﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorScript : MonoBehaviour
{
    public GameObject pointer;
    GameObject currentPointer;
    //bool isSpawned = false;

    // Start is called before the first frame update

    // Update is called once per frame
    public void SpawnPointer(Vector3 position)
    {
        //if (isSpawned==true)
        //{
        //    Destroy(currentPointer);
        //    isSpawned = false;
        //}
        //else
        //{
        //currentPointer = pointer;
        //GameObject.Instantiate(currentPointer, position, Quaternion.identity);
        //isSpawned = true;
        //}
        GameObject.Instantiate(pointer, position, Quaternion.identity);
    }

    public void DestroyPointer()
    {
        Destroy(currentPointer);
        //isSpawned = false;
    }

}
